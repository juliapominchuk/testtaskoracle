#ifndef _TEST_HPP_
#define _TEST_HPP_

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include "MemoryManager.hpp"

typedef void(*TestFuntion)();

/*****************************************************************************/

class TestsRunner
{

/*----------------------------------------------------------------------------*/

public:

/*----------------------------------------------------------------------------*/

void addTest(
  std::string const & _testName, TestFuntion _testFunction
)
{
  m_testFunctions.push_back(std::make_pair(_testName, _testFunction));
}

/*----------------------------------------------------------------------------*/

void runTests()
{
  std::cout << "Start running " << m_testFunctions.size() << " test(s):\n";

  int testNumber = 1;
  std::for_each(
      m_testFunctions.begin()
    , m_testFunctions.end()
    , [&](std::pair< std::string, TestFuntion > const & _test)
    {
      std::cout << "Test #" << testNumber++ << " \"" << _test.first << "\n";
      (*_test.second)();
      std::cout << '\n';
    }
  );

  std::cout << "Finish.\n";
}

/*----------------------------------------------------------------------------*/

static long* g_preAllocatedMem;

static std::vector< void * > g_allocatedMem;

/*----------------------------------------------------------------------------*/

private:

/*----------------------------------------------------------------------------*/

  std::vector< std::pair<std::string, TestFuntion> > m_testFunctions;

/*----------------------------------------------------------------------------*/

};


/*****************************************************************************/


static TestsRunner gs_TestsRunner;


/*****************************************************************************/


class TestProcedureWrapper
{
public:
  TestProcedureWrapper(
    std::string const & _testName, TestFuntion _testFunction
  )
  {
    gs_TestsRunner.addTest(_testName, _testFunction);
  }
};


/*****************************************************************************/


#define MEMORY_MANAGER_TEST( testName )                                         \
    void testName ();                                                           \
    static TestProcedureWrapper gs_wrapper_##testName( #testName, & testName ); \
    void testName ()


/*****************************************************************************/

int main()
{
  TestsRunner::g_preAllocatedMem = new long;
  gs_TestsRunner.runTests();

  delete TestsRunner::g_preAllocatedMem;
}

#endif