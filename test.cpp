#include "test.hpp"

/*----------------------------------------------------------------------------*/

long* TestsRunner::g_preAllocatedMem;
std::vector< void * > TestsRunner::g_allocatedMem;

/*----------------------------------------------------------------------------*/

void printResult( const bool _condition )
{
    if ( _condition )
        std::cout<<"Test passed\n";
    else
        std::cout<<"Test failed\n";
}

/*----------------------------------------------------------------------------*/

void initVector( const int _blockCount, MemoryManager & _manager )
{
    for ( int i = 0; i < _blockCount; ++i )
    {
       TestsRunner::g_allocatedMem.push_back( _manager.allocate() );
    }
}

/*----------------------------------------------------------------------------*/

void freeVector( const int _blockCount, MemoryManager & _manager )
{
    for ( int i = 0; i < _blockCount; ++i )
    {
       _manager.free( TestsRunner::g_allocatedMem[i] );
    }
}

/*----------------------------------------------------------------------------*/

MEMORY_MANAGER_TEST(CArray_SizeOutOfMemory)
{
    MemoryManager memoryManager( TestsRunner::g_preAllocatedMem, 4, 8 );

    void* result = memoryManager.allocate();

    printResult( result == nullptr );

    memoryManager.free(result);
}

/*----------------------------------------------------------------------------*/

MEMORY_MANAGER_TEST(CArray_AllocateCorrectSize)
{
    MemoryManager memoryManager( TestsRunner::g_preAllocatedMem, 32, 4 );

    void* result = memoryManager.allocate();

    printResult( result != nullptr );

    memoryManager.free(result);
}

/*----------------------------------------------------------------------------*/

MEMORY_MANAGER_TEST(CArray_Allocate10Blocks)
{
    MemoryManager memoryManager( TestsRunner::g_preAllocatedMem, 64, 8 );

    initVector( 8, memoryManager );
    void* result = memoryManager.allocate();

    printResult( result == nullptr );

    memoryManager.free(result);
    freeVector( 8, memoryManager );
}

/*----------------------------------------------------------------------------*/

MEMORY_MANAGER_TEST(CArray_FreeBlock)
{
    MemoryManager memoryManager( TestsRunner::g_preAllocatedMem, 32, 8 );

    initVector( 4, memoryManager );
    void* firstBlock = TestsRunner:: g_allocatedMem[0];

    memoryManager.free( firstBlock );
    void* result = memoryManager.allocate();

    printResult( result == firstBlock );

    memoryManager.free(result);
    freeVector( 4, memoryManager );
}

/*----------------------------------------------------------------------------*/

MEMORY_MANAGER_TEST(CArray_NullBlockSize)
{
    try {
        MemoryManager memoryManager( TestsRunner::g_preAllocatedMem, 32, 0 );
        void* result = memoryManager.allocate();
        std::cout<<"Test failed\n";
    }
    catch ( const std::invalid_argument& e )
    {
        std::cout<<"Test passed\n";
    }
}

/*----------------------------------------------------------------------------*/

MEMORY_MANAGER_TEST(CArray_NullPreAllocMem)
{
    try {
        MemoryManager memoryManager( nullptr, 32, 4 );
        void* result = memoryManager.allocate();
        std::cout<<"Test failed\n";
    }
    catch ( const std::invalid_argument& e )
    {
        std::cout<<"Test passed\n";
    }
}

/*----------------------------------------------------------------------------*/

MEMORY_MANAGER_TEST(CArray_NullAvialable)
{
    try {
        MemoryManager memoryManager( TestsRunner::g_preAllocatedMem, 0, 4 );
        void* result = memoryManager.allocate();
        std::cout<<"Test failed\n";
    }
    catch ( const std::invalid_argument& e )
    {
        std::cout<<"Test passed\n";
    }
}

/*----------------------------------------------------------------------------*/