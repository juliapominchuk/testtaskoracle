#ifndef _BIT_SET_HPP_
#define _BIT_SET_HPP_

class BitSet {

/*----------------------------------------------------------------------------*/

public:

    BitSet( int _blocksNumber );

    ~BitSet();

/*----------------------------------------------------------------------------*/

    void setOccupied( int _blockNumber );
     
    void setAvailable( int _blockNumber );

    bool isAvailable( int _blockNumber );

/*----------------------------------------------------------------------------*/

private:

    int getCellNumber( int _blockNumber );

    int getBitNumber( int _blockNumber );

    int* m_occupiedBlocksInfo;

    const int INT_SIZE;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

#endif