#ifndef _MEMORY_MANAGER_HPP_
#define _MEMORY_MANAGER_HPP_

#include <cstddef>

class BitSet;

/*----------------------------------------------------------------------------*/

class MemoryManager{

/*----------------------------------------------------------------------------*/

 public:

    MemoryManager(
            void* _preAllocatedMemory
        ,   const size_t _avilableMemory
        ,   const size_t _blockSize
    );

    ~MemoryManager ();

    void* allocate();

    void free(void* pointerToDelete);

/*----------------------------------------------------------------------------*/

private:

    BitSet* m_set;

    void* m_preAllocatedMemory;

    const size_t m_avialableMemory;
    const size_t m_blockSize;
    size_t m_blocksCount;

/*----------------------------------------------------------------------------*/

};

/*----------------------------------------------------------------------------*/

#endif