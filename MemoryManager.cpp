#include "MemoryManager.hpp"
#include "BitSet.hpp"

#include <cstdlib>
#include <stdexcept>
#include <new>

/*----------------------------------------------------------------------------*/

MemoryManager::MemoryManager(
        void* _preAllocatedMemory
    ,   const size_t _avilableMemory
    ,   const size_t _blockSize
)   :   m_preAllocatedMemory( _preAllocatedMemory )
    ,   m_avialableMemory( _avilableMemory )
    ,   m_blockSize( _blockSize )
{
	if (	_blockSize <= 0
		||	_avilableMemory <= 0
		||	!_preAllocatedMemory
	)
		throw std::invalid_argument( "received invalid value" );

	m_blocksCount = _avilableMemory / _blockSize;

	void* setMemory = std::malloc(sizeof(BitSet));
	m_set = new (setMemory) BitSet(m_blocksCount);
}

/*----------------------------------------------------------------------------*/

MemoryManager::~MemoryManager()
{
	std::free( m_set );
}

/*----------------------------------------------------------------------------*/

void* MemoryManager::allocate()
{
	if ( m_blockSize <= 0 )
		return nullptr;

	void* currentLocation = m_preAllocatedMemory;
	void* memoryLocation = nullptr;

	for ( int i = 0; i < m_blocksCount; ++i )
	{
		if( m_set->isAvailable( i ) )
		{
			m_set->setOccupied( i );
			memoryLocation = currentLocation;

			break;
		}

		currentLocation = static_cast< char* >( currentLocation ) + m_blockSize;
	}

	return memoryLocation;
}

/*----------------------------------------------------------------------------*/

void MemoryManager::free(void* _pointerToDelete)
{
	if (
			_pointerToDelete < m_preAllocatedMemory
		||	(	static_cast< char* >( m_preAllocatedMemory )
			>	static_cast< char* >( m_preAllocatedMemory ) + m_avialableMemory )
	)
		return;

	char* blockStart = static_cast< char* >( _pointerToDelete );
	char* memoryStart = static_cast< char* >( m_preAllocatedMemory );
	int blockNumber = ( blockStart - memoryStart ) / m_blockSize;

	m_set->setAvailable( blockNumber );
}

/*----------------------------------------------------------------------------*/