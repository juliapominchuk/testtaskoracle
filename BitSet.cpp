#include <cstdlib>
#include <limits>
#include <new>

#include "BitSet.hpp"

/*----------------------------------------------------------------------------*/

BitSet::BitSet( int _blocksNumber )
    :   INT_SIZE ( std::numeric_limits<int>::digits + 1 ) // numeric limit for int in digits is 31, so int_size = 31 + 1
{
    void* setMemory = std::malloc(sizeof(int) * _blocksNumber);
    m_occupiedBlocksInfo = new (setMemory) int[_blocksNumber]();

    for( int i = 0; i < _blocksNumber; ++i )
	   m_occupiedBlocksInfo[i] = 0;
}

/*----------------------------------------------------------------------------*/

BitSet::~BitSet()
{
    std::free( m_occupiedBlocksInfo );
}

/*----------------------------------------------------------------------------*/

void BitSet::setOccupied( int _blockNumber )
{
    m_occupiedBlocksInfo[getCellNumber( _blockNumber )] |= 1 << getBitNumber(_blockNumber);
}

/*----------------------------------------------------------------------------*/

void BitSet::setAvailable( int _blockNumber )
{
    m_occupiedBlocksInfo[getCellNumber( _blockNumber )] &= ~( 1 << getBitNumber(_blockNumber) );
}

/*----------------------------------------------------------------------------*/

bool BitSet::isAvailable( int _blockNumber )
{
    return !( m_occupiedBlocksInfo[getCellNumber( _blockNumber )] & ( 1 << getBitNumber(_blockNumber) ) );
}

/*----------------------------------------------------------------------------*/

int BitSet::getCellNumber( int _blockNumber )
{
    return _blockNumber / INT_SIZE;
}

/*----------------------------------------------------------------------------*/

int BitSet::getBitNumber( int _blockNumber )
{
    return _blockNumber - INT_SIZE * getCellNumber( _blockNumber );
}

/*----------------------------------------------------------------------------*/